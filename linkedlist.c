#include "linkedlist.h"
#include <stdlib.h>

int main()
{
    struct Node *n;
    n = malloc(1*sizeof(struct Node));
    //n.value = 12;  // no good: n is a ptr
    (*n).value = 12; // approach 1: good but clunky
    n->value = 12;  // approach 2: same thing but pretty
}
