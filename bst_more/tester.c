#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "utils.h"
#include "bst.h"

int main() {
    /*Value *value = malloc(sizeof(Value));
    value->type = INTTYPE;
    value->intValue = 3;

    Value *value2 = malloc(sizeof(Value));
    value2->type = STRTYPE;
    value2->strValue = malloc(sizeof(10*sizeof(char)));
    strcpy(value2->strValue,"hey");

    ConsCell *cell = malloc(sizeof(ConsCell));
    cell->car = value;
    cell->cdr = value2;

    printValue(value);
    printValue(value2);
    printConsCell(cell);

    myfree(cell);
    myfree(value2->strValue);
    myfree(value2);
    myfree(value);*/

    Value *bst = nullBst();

    Value *value1 = malloc(sizeof(Value));
    value1->type = INTTYPE;
    value1->intValue = 3;

    bst = makeBst(value1,NULL,NULL);
    bst = insertBstInteger(bst,12);
    bst = insertBstInteger(bst,1);
    bst = insertBstInteger(bst,2);
    printf("here it is\n");
    printValue(bst);
    printTree(bst,0);



    freeBst(bst);

    /*printf("Before inserting...\n");
    printTree(bst);
    bst = insertBstInteger(bst,12);
    bst = insertBstInteger(bst,6);
    printf("After inserting...\n");
    printTree(bst);
    freeBst(bst);*/
}
