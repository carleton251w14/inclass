#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "bst.h"


Value *nullBst() {
    return NULL;
}


Value *makeBst(Value *elt, Value *left, Value *right) {
    Value *bst = malloc(sizeof(Value));
    bst->type = CONSTYPE;
    bst->cons = malloc(sizeof(ConsCell));
    bst->cons->car = elt;
    bst->cons->cdr = malloc(sizeof(Value));
    Value *children = bst->cons->cdr;
    children->type = CONSTYPE;
    children->cons = malloc(sizeof(ConsCell));
    children->cons->car = left;
    children->cons->cdr = right;
    return bst;
}


Value *insertBstInteger(Value *bst, int entry) {
    if (bst == NULL) {
        Value *valueEntry = malloc(sizeof(Value));
        valueEntry->type = INTTYPE;
        valueEntry->intValue = entry;
        return makeBst(valueEntry, NULL, NULL);
    } else {
        assert(bst->type == CONSTYPE);
        assert(bst->cons != NULL);
        assert(bst->cons->car != NULL);
        assert(bst->cons->car->type == INTTYPE);
        int current = bst->cons->car->intValue;
        Value *children = bst->cons->cdr;
        assert (children != NULL);
        assert (children->type == CONSTYPE);
        if (entry <= current) {
            children->cons->car = insertBstInteger(children->cons->car, entry);
        } else {
            children->cons->cdr = insertBstInteger(children->cons->cdr, entry);
        }
        return bst;
    }
}
            




void freeBst(Value *bst) {
    if (bst != NULL) {
        assert(bst->type == CONSTYPE);
        assert(bst->cons != NULL);
        Value *children = bst->cons->cdr;
        assert(children->type == CONSTYPE);
        assert(children->cons != NULL);
        freeBst(children->cons->car);
        freeBst(children->cons->cdr);
        myfree(children->cons);
        myfree(children);
        myfree(bst->cons->car);
        myfree(bst->cons);
        myfree(bst);
        
    }
}


void printTree(Value *bst, int indentLevel) {
    if (bst == NULL) {
        for (int i=0; i < indentLevel; i++)
            printf(" ");
        printf("Null tree.\n");
    } else {
        for (int i=0; i < indentLevel; i++)
            printf(" ");
        printf("Address of tree is = %p\n",bst);
        assert(bst->type == CONSTYPE);
        assert(bst->cons != NULL);
        for (int i=0; i < indentLevel; i++)
            printf(" ");
        printValue(bst->cons->car);
        Value *children = bst->cons->cdr;
        assert(children->type == CONSTYPE);
        assert(children->cons != NULL);
        printTree(children->cons->car,indentLevel+1);
        //printf("%i ",bst->cons->car->intValue);
        printTree(children->cons->cdr,indentLevel+1);
    }
}
