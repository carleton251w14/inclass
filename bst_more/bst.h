#include "utils.h"

Value *nullBst();
Value *insertBstInteger(Value *bst, int entry);
void freeBst(Value *value);
void printTree(Value *bst, int indentLevel);
Value *makeBst(Value *elt, Value *left, Value *right);

