#include <stdio.h>
#include <stdlib.h>
#include "utils.h"


void printValue(Value *value) {
    printf("--------------\n");
    printf("Value address = %p\n",value);
    if (value == NULL) {
        printf("Value is null.\n");
        printf("--------------\n");
        return;
    }
    
    switch (value->type) {
    case INTTYPE:
        printf("Integer. Content = %i\n",value->intValue);
        break;
    case STRTYPE:
        printf("String. String address = %p\n",value->strValue);
        if (value->strValue == NULL) {
            printf("String is null.\n");
        } else {
            printf("String. Content is %s\n",value->strValue);
        }
        break;
    case CONSTYPE:
        printf("ConsCell. Address = %p\n",value->cons);
        
    }
    printf("--------------\n");
}



void printConsCell(ConsCell *cons) {
    printf("--------------\n");
    printf("ConsCell address = %p\n",cons);
    if (cons == NULL) {
        printf("ConsCell is null.\n");
        printf("--------------\n");
        return;
    }

    printf("Car address = %p\n",cons->car);
    printf("Cdr address = %p\n",cons->cdr);
    printf("--------------\n");
}


void myfree(void *ptr) {
    printf("Freeing address %p\n",ptr);
    free(ptr);
}
