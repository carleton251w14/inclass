#ifndef _UTILS
#define _UTILS

typedef enum {INTTYPE, STRTYPE, CONSTYPE} valueType;

typedef struct Value {
    valueType type;
    union {
        int intValue;
        char *strValue;
        struct ConsCell *cons;
    };
} Value;


typedef struct ConsCell {
    struct Value *car;
    struct Value *cdr;
} ConsCell;


void printValue(Value *value);
void printConsCell(ConsCell *cons);
void myfree(void *ptr);
        
#endif
