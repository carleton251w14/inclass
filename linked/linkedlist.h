struct LinkedList
{
    int value;
    struct LinkedList *next;
};

typedef struct LinkedList LinkedList;

LinkedList *insertLL(LinkedList *list, int newValue);
void cleanupLL(LinkedList *list);
