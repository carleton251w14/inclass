#include <stdlib.h>
#include "linkedlist.h"

LinkedList *insertLL(LinkedList *list, int newValue)
{
    // This will get freed in cleanupLL
    LinkedList *newCell = malloc(sizeof(LinkedList));
    newCell->value = newValue;
    newCell->next = list;
    return newCell;
}

void cleanupLL(LinkedList *list)
{
    LinkedList *cur = list;
    while (cur != NULL)
    {
        LinkedList *toGo = cur;
        cur = cur->next;
        free(toGo);
    }
}
    
    
