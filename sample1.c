#include <stdio.h>

int main()
{
    int x = 3;
    int y = x;
    printf("%i %i\n",x,y);
    printf("The address of x is %p\n",&x);
    printf("The address of y is %p\n",&y);
    printf("Equal? %i\n", x==y);
    printf("Equal? %i\n", &x==&y);

    int *z;  // z is a pointer to an integer
    z = &x;
    x = 23;
    printf("%i\n",z);
    printf("%i\n",*z);

}
