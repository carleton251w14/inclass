#include <stdio.h>
#include <stdlib.h>

int main()
{
    int t;
    t = -15;
    printf("# of bytes in int is %i\n",sizeof(int));
    printf("terrible weather is %i\n",t);


    int numbers[5]; // array of 10 ints
    
    int i;
    for (i=0; i < 5; i++)
    {
        numbers[i] = i+10;
        printf("Value %i is %i\n",i,numbers[i]);
    }
    
    // Hmmmm....
    printf("A number: %i\n",*numbers);
    printf("A number: %i\n",*(numbers+1));

    printf("A number: %p\n",numbers);
    printf("A number: %p\n",(numbers+1));

    // Let's do evil
    //printf("A bad number %i\n",numbers[10]);


    // Now, a heap array
    int *cscourses;
    cscourses = (int*)malloc(5*sizeof(int));
    
    cscourses[0] = 117;
    cscourses[1] = 217;

    printf("First course is %i\n",cscourses[0]);
    // clean up old array first
    free(cscourses);
    //printf("First course again is %i\n",cscourses[0]);

    cscourses = (int*)malloc(5*sizeof(int));

    cscourses[0] = 111;
    cscourses[1] = 251;

    printf("First course is %i\n",cscourses[0]);

    free(cscourses);

    return 0;
}
