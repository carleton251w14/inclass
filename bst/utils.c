#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

void printValue(Value *value) {
    printf("Value!!!\n");
    printf("Pointer address = %p\n",value);
    assert(value != NULL);
    printf("Type = %i\n",value->type);
    if (value->type == INTTYPE) {
        printf("Integer value = %d\n",value->intValue);
    }
}

void myfree(void *ptr) {
    printf("Freeing memory location %p\n",ptr);
    free(ptr);
}
