typedef enum {INTTYPE, CONSTYPE} valueType;

typedef struct Value {
    valueType type;
    union {
        int intValue;
        struct ConsCell *cons;
    };
} Value;

typedef struct ConsCell {
    struct Value *car;
    struct Value *cdr;
} ConsCell;

// function prototypes
void printValue(Value *value);
void myfree(void *ptr);
