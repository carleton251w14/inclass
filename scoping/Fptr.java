interface Function {
    public int f(int x, int y);
}
    

public class Fptr {

    public static int doit(Function fobj, int x, int y) {
        return fobj.f(x,y);
    }

    public static void main(String[] args) {

        System.out.println(doit(
          new Function() {
              public int f (int x, int y) {
                  return x + y;
              }
          },3,5));
    }
}
